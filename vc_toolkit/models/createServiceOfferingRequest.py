from typing import Optional
from pydantic import BaseModel, Field

class CreateServiceOfferingRequest(BaseModel):
    lrn_vc_id: str
    lp_vc_id: str
    tc_vc_id: str
    name: str
    serviceEndpointUrl: str
    termsAndConditionsUrl: str
    requestType: Optional[str] = Field("API")
    formatType: Optional[str] = Field("application/json")
    policy: Optional[str] = Field("default allow = true")